const express = require('express');

const router = express.Router();

const {
  getAllLoads, addLoad, getActiveLoad, iterateToLoadState,
  getLoad, updateLoad, deleteLoad, postLoad, getShipInfo,
} = require('../controller/loads_controller');
const { authMiddleware } = require('../middleware/authMiddleware');
// const { driverMiddleware } = require('../middleware/driverMiddleware');

router.get('/', authMiddleware, getAllLoads);

router.post('/', authMiddleware, addLoad);

router.get('/active', authMiddleware, getActiveLoad);

router.patch('/active/state', authMiddleware, iterateToLoadState);

router.get('/:id', authMiddleware, getLoad);

router.put('/:id', authMiddleware, updateLoad);

router.delete('/:id', authMiddleware, deleteLoad);

router.post('/:id/post', authMiddleware, postLoad);

router.get('/:id/shipping_info', authMiddleware, getShipInfo);

module.exports = {
  loadsRouter: router,
};
