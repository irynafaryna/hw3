const express = require('express');
const { body } = require('express-validator');

const router = express.Router();
const { registerUser, loginUser, passwoordReset } = require('../controller/auth_controller');

router.post('/register', body('email').isEmail(), body('password').isLength({ min: 6 }), registerUser);

router.post('/login', loginUser);

router.post('/forgot_password', passwoordReset);

module.exports = {
  authRouter: router,
};
