const express = require('express');

const router = express.Router();

const {
  getAllTrucks, addTruck, getTruck, updateTruck, deleteTruck, assignTruck,
} = require('../controller/trucks_controller');
const { authMiddleware } = require('../middleware/authMiddleware');
// const { driverMiddleware } = require('../middleware/driverMiddleware');

router.get('/', authMiddleware, getAllTrucks);

router.post('/', authMiddleware, addTruck);

router.get('/:id', authMiddleware, getTruck);

router.put('/:id', authMiddleware, updateTruck);

router.delete('/:id', authMiddleware, deleteTruck);

router.post('/:id/assign', authMiddleware, assignTruck);

module.exports = {
  trucksRouter: router,
};
