const express = require('express');

const router = express.Router();

const { getProfileInfo, changePassword, deleteProfile } = require('../controller/users_controller');
const { authMiddleware } = require('../middleware/authMiddleware');

router.get('/', authMiddleware, getProfileInfo);

router.patch('/password', authMiddleware, changePassword);

router.delete('/', authMiddleware, deleteProfile);

module.exports = {
  usersRouter: router,
};
