const mongoose = require('mongoose');

const Truck = mongoose.model('truck', {
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    required: false,
  },
  loadId: {
    type: String,
    required: false,
  },
  type: {
    type: String,
    enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
    required: true,
  },
  payload: {
    type: Number,
    required: false,
  },
  dimensions: {
    type: Object,
    width: {
      type: Number,
      required: true,
    },
    length: {
      type: Number,
      required: true,
    },
    height: {
      type: Number,
      required: true,
    },
  },
  status: {
    type: String,
    enum: ['OL', 'IS'],
    default: 'IS',
  },
  createdDate: {
    type: String,
    format: Date,
    default: new Date().toISOString(),
  },
});

module.exports = {
  Truck,
};
