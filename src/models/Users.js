const mongoose = require('mongoose');

const User = mongoose.model('user', {
  role: {
    type: String,
    enum: ['SHIPPER', 'DRIVER'],
    required: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  createdDate: {
    type: String,
    format: Date,
    default: new Date().toISOString(),
  },
  truckId: {
    type: String,
    required: false,
  },
});

module.exports = {
  User,
};
