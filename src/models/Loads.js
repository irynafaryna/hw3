const mongoose = require('mongoose');

const Load = mongoose.model('load', {
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    default: null,
  },
  driverId: {
    type: String,
    required: false,
  },
  status: {
    type: String,
    enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
    default: 'NEW',
  },
  state: {
    type: String,
    enum: ['En route to Pick Up', 'Arrived to Pick Up', 'En route to delivery', 'Arrived to delivery'],
    required: false,
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    type: Object,
    width: {
      type: Number,
      required: true,
    },
    length: {
      type: Number,
      required: true,
    },
    height: {
      type: Number,
      required: true,
    },
  },
  logs: {
    type: Array,
    required: false,
    items: {
      type: Object,
      required: true,
      message: {
        type: String,
        required: true,
      },
      time: {
        type: String,
        format: Date,
        default: new Date().toISOString(),
      },
    },
  },
  createdDate: {
    type: String,
    format: Date,
    default: new Date().toISOString(),
  },
});

module.exports = {
  Load,
};
