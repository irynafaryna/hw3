const bcrypt = require('bcryptjs');
const { User } = require('../models/Users');

const getProfileInfo = async (req, res) => {
  try {
    const { userId } = req.user;

    if (userId) {
      return await User.findOne({ _id: userId })
        .then((user) => {
          res.status(200).json({
            user: {
              _id: userId,
              role: user.role,
              email: user.email,
              created_date: user.createdDate,
            },
          });
        });
    }
    res.status(400).json({ message: 'Bad request' });
  } catch (err) {
    return res.status(500).send({ message: 'Server error' });
  }
};

const changePassword = async (req, res) => {
  try {
    const { userId } = req.user;
    const user = await User.findOne({ _id: userId });
    const { oldPassword, newPassword } = req.body;
    if (newPassword && bcrypt.compare(oldPassword, user.password) && typeof oldPassword === 'string' && typeof newPassword === 'string') {
      const passwordNew = await bcrypt.hash(newPassword, 10);
      await User.findOneAndUpdate({ _id: userId }, { password: passwordNew }).then(() => res.status(200).json({ message: 'Password changed successfully' }));
    } else {
      res.status(400).json({ message: 'Bad request' });
    }
  } catch (err) {
    return res.status(500).send({ message: 'Server error' });
  }
};

const deleteProfile = async (req, res) => {
  try {
    const { userId } = req.user;
    if (userId) {
      await User.findOneAndDelete({ _id: userId })
        .then(() => {
          res.status(200).json({
            message: 'Profile deleted successfully',
          });
        });
    } else {
      res.status(400).json({ message: 'Bad request' });
    }
  } catch (err) {
    return res.status(500).send({ message: 'Server error' });
  }
};

module.exports = {
  getProfileInfo,
  changePassword,
  deleteProfile,
};
