const bcrypt = require('bcryptjs');
const { validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');
const { User } = require('../models/Users');
const sendEmail = require('../utils/sendEmail');

const registerUser = async (req, res) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    const { email, password, role } = req.body;
    const userEx = await User.findOne({ email });
    if (userEx) {
      res.status(400).send({ message: 'User exist' });
    }
    const user = new User({
      email,
      password: await bcrypt.hash(password, 10),
      role,
    });
    await user.save();
    res.status(200).send({ message: 'Profile created successfully' });
  } catch (err) {
    return res.status(500).send({ message: 'Internal server error' });
  }
};

const loginUser = async (req, res) => {
  try {
    const user = await User.findOne({ email: req.body.email });
    if (!req.body.email || !req.body.password || typeof req.body.password !== 'string' || typeof req.body.email !== 'string') {
      res.status(400).json({ message: 'Bad request' });
    }
    if (user && await bcrypt.compare(String(req.body.password), String(user.password))) {
      // eslint-disable-next-line
      const payload = { email: user.email, userId: user._id };
      const jwtToken = jwt.sign(payload, 'secret-jwt-key');
      return res.status(200).json({ jwt_token: jwtToken });
    }
    return res.status(400).json({ message: 'Not authorized' });
  } catch (err) {
    return res.status(500).send({ message: 'Internal server error' });
  }
};

const passwoordReset = async (req, res) => {
  try {
    const user = await User.findOne({ email: req.body.email });
    if (!req.body.email || typeof req.body.email !== 'string' || !user) {
      res.status(400).json({ message: 'Bad request' });
    }
    const randPassword = Array(10).fill('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz')
      .map((x) => x[Math.floor(Math.random() * x.length)]).join('');

    await sendEmail(user.email, 'Password reset', randPassword);
    res.status(200).send({ message: 'New password sent to your email address' });
  } catch (err) {
    return res.status(500).send({ message: 'Internal server error' });
  }
};

module.exports = {
  registerUser,
  loginUser,
  passwoordReset,
};
