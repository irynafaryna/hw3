const { User } = require('../models/Users');
const { Truck } = require('../models/Trucks');

const getAllTrucks = async (req, res) => {
  try {
    const { userId } = req.user;
    const driver = await User.findOne({ _id: userId });
    if (driver.role !== 'DRIVER') {
      return res.status(400).json({ message: 'User is not a driver' });
    }

    const trucks = await Truck.find({
      created_by: userId,
    });
    return res.status(200).send({
      trucks,
    });
  } catch (err) {
    return res.status(500).send({ message: 'Server error' });
  }
};

const addTruck = async (req, res) => {
  try {
    const { type } = req.body;

    const { userId } = req.user;
    const driver = await User.findOne({ _id: userId });
    if (driver.role !== 'DRIVER') {
      return res.status(400).json({ message: 'User is not a driver' });
    }

    let [width, length, height] = [];
    let payload;
    if (!type || !userId || typeof type !== 'string') {
      return res.status(400).json({ message: 'Bad request' });
    }

    if (type === 'SPRINTER') {
      [width, length, height] = [300, 250, 170];
      payload = 1700;
    }

    if (type === 'SMALL STRAIGHT') {
      [width, length, height] = [500, 250, 170];
      payload = 2500;
    }

    if (type === 'LARGE STRAIGHT') {
      [width, length, height] = [700, 350, 200];
      payload = 4000;
    }

    const truck = new Truck({
      created_by: userId,
      type,
      dimensions: {
        width,
        length,
        height,
      },
      payload,
    });

    return await truck.save().then(() => {
      res.status(200).json({
        message: 'Truck created successfully',
      });
    });
  } catch (err) {
    return res.status(500).send({ message: 'Internal server error' });
  }
};

const getTruck = async (req, res) => {
  try {
    const { userId } = req.user;
    const driver = await User.findOne({ _id: userId });
    if (driver.role !== 'DRIVER') {
      return res.status(400).json({ message: 'User is not a driver' });
    }
    if (req.params.id) {
      return await Truck.findById(req.params.id)
        .then((truck) => {
          res.status(200).json({
            truck,
          });
        });
    }
    res.status(400).json({ message: 'Bad request' });
  } catch (err) {
    return res.status(500).send({ message: 'Server error' });
  }
};

const updateTruck = async (req, res) => {
  try {
    const { userId } = req.user;
    const driver = await User.findOne({ _id: userId });
    if (driver.role !== 'DRIVER') {
      return res.status(400).json({ message: 'User is not a driver' });
    }

    const thisTruck = await Truck.findById(req.params.id);

    if (thisTruck.status === 'OL') {
      return res.status(400).json({ message: 'This truck has status "OL"' });
    }

    if (thisTruck.assigned_to === userId) {
      res.status(400).json({ message: 'This truck is assigned to you' });
    }

    const { type } = req.body;
    if (!type || typeof type !== 'string') {
      return res.status(400).json({ message: 'Bad request' });
    }
    return await Truck.findOneAndUpdate({ _id: req.params.id }, { type })
      .then(() => res.status(200).json({ message: 'Truck details changed successfully' }));
  } catch (err) {
    return res.status(500).send({ message: 'Server error' });
  }
};

const deleteTruck = async (req, res) => {
  try {
    const { userId } = req.user;
    const driver = await User.findOne({ _id: userId });
    if (driver.role !== 'DRIVER') {
      return res.status(400).json({ message: 'User is not a driver' });
    }

    const thisTruck = await Truck.findById(req.params.id);
    if (thisTruck.assigned_to === userId) {
      return res.status(400).json({ message: 'This truck is assigned to you' });
    }

    if (req.params.id) {
      await Truck.findByIdAndDelete(req.params.id)
        .then(() => {
          res.status(200).json({ message: 'Truck deleted successfully' });
        });
    } else {
      return res.status(400).json({ message: 'Bad request' });
    }
  } catch (err) {
    return res.status(500).send({ message: 'Server error' });
  }
};

const assignTruck = async (req, res) => {
  try {
    if (!req.params.id) {
      return res.status(400).json({ message: 'Bad request' });
    }
    const { userId } = req.user;
    const driver = await User.findOne({ _id: userId });
    if (driver.role !== 'DRIVER') {
      return res.status(400).json({ message: 'User is not a driver' });
    }

    const thisTruck = await Truck.findById(req.params.id);
    if (thisTruck.status === 'OL') {
      return res.status(400).json({ message: 'This truck has status "OL"' });
    }

    if (thisTruck.assigned_to) {
      return res.status(400).json({ message: 'This truck is already assigned' });
    }
    if (driver.truckId && driver.truckId !== null) {
      return res.status(400).json({ message: 'Driver has already truck' });
    } else {
      await User.findOneAndUpdate( {_id: userId }, { truckId: req.params.id });
      return await Truck.findOneAndUpdate({ _id: req.params.id }, { assigned_to: userId })
      .then(() => res.status(200).json({ message: 'Truck assigned successfully' }));
    }  
  } catch (err) {
    return res.status(500).send({ message: 'Server error' });
  }
};

module.exports = {
  getAllTrucks,
  addTruck,
  getTruck,
  updateTruck,
  deleteTruck,
  assignTruck,
};
