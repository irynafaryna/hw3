const { User } = require('../models/Users');
const { Load } = require('../models/Loads');
const { Truck } = require('../models/Trucks');

const getAllLoads = async (req, res) => {
  try {
    const { userId } = req.user;
    const user = await User.findById(userId);
    const { status } = req.query;
    let { offset, limit } = req.query;
    offset = !offset ? '0' : offset;
    limit = !limit ? '10' : limit;
    limit = +limit > 50 ? '50' : limit;

    if (!userId) {
      return res.status(400).send({ message: 'Bad request' });
    }

    if (!status && user.role === 'DRIVER') {
      return await Load.find({
        driverId: userId,
      }).skip(+offset)
        .limit(+limit)
        .then((loads) => {
          res.status(200).json({
            loads,
          });
        });
    }

    if (!status && user.role === 'SHIPPER') {
      return await Load.find({
        created_by: userId,
      }).skip(+offset)
        .limit(+limit)
        .then((loads) => {
          res.status(200).json({
            loads,
          });
        });
    }

    if ((status === 'ASSIGNED' || status === 'SHIPPED') && user.role === 'DRIVER') {
      return await Load.find({
        status: status,
        driverId: userId,
      }).skip(+offset)
        .limit(+limit)
        .then((loads) => {
          res.status(200).json({
            loads,
          });
        });
    }
    if ((status === 'NEW' || status === 'POSTED') && user.role === 'DRIVER') {
      return res.status(400).send({ message: 'Driver has not access to new or posted loads.' });
    }
    if ((status === 'SHIPPED' || status === 'ASSIGNED') && user.role === 'SHIPPER') {
      return res.status(400).send({ message: 'Shipper has not access to assigned or shipped loads.' });
    }
    if ((status === 'NEW' || status === 'POSTED') && user.role === 'SHIPPER') {
      return await Load.find({
        status: status,
      }).skip(+offset)
        .limit(+limit)
        .then((loads) => {
          res.status(200).json({
            loads,
          });
        });
    }
  } catch (err) {
    return res.status(500).send({ message: 'Server error' });
  }
};

const addLoad = async (req, res) => {
  try {
    const { userId } = req.user;
    const shipper = await User.findOne({ _id: userId });
    if (shipper.role !== 'SHIPPER') {
      return res.status(400).json({ message: 'User is not a shipper' });
    }

    const {
      name, payload, pickup_address, delivery_address, dimensions,
    } = req.body;
    const { width, length, height } = dimensions;

    if (!name || !payload || !pickup_address || !delivery_address || !dimensions
      || !width || !length || !height || !userId || typeof name !== 'string'
      || typeof payload !== 'number' || typeof pickup_address !== 'string' || typeof delivery_address !== 'string'
      || typeof dimensions !== 'object' || typeof width !== 'number' || typeof length !== 'number'
      || typeof height !== 'number') {
      return res.status(400).json({ message: 'Bad request' });
    }
    const load = new Load({
      created_by: userId,
      name,
      payload,
      pickup_address,
      delivery_address,
      dimensions: {
        width,
        length,
        height,
      },
    });
    return await load.save().then(() => {
      res.status(200).json({
        message: 'Load created successfully',
      });
    });
  } catch (err) {
    return res.status(500).send({ message: 'Internal server error' });
  }
};

const getActiveLoad = async (req, res) => {
  try {
    const { userId } = req.user;
    const driver = await User.findOne({ _id: userId });
    if (driver.role !== 'DRIVER') {
      return res.status(400).json({ message: 'User is not a driver' });
    }

    const load = await Load.findOne({ driverId: userId,  });
    if (load) {
      return res.status(200).json({load});
    } else {
      return res.status(400).json({ message: 'No active loads' });
    }
  } catch (err) {
    return res.status(500).send({ message: 'Server error' });
  }
};

const iterateToLoadState = async (req, res) => {
  try {
    const { userId } = req.user;
    const driver = await User.findOne({ _id: userId });
    if (driver.role !== 'DRIVER') {
      return res.status(400).json({ message: 'User is not a driver' });
    }

    const load = await Load.findOne({ driverId: userId, status: 'ASSIGNED' });
    if (load.state === 'En route to Pick Up') {
      await Load.findOneAndUpdate({ _id: load.id }, { state: 'Arrived to Pick Up' });
      return res.status(200).json({ message: "Load state changed to 'Arrived to Pick Up'" });
    } 
    if (load.state === 'Arrived to Pick Up') {
      await Load.findOneAndUpdate({ _id: load.id }, { state: 'En route to delivery' });
      return res.status(200).json({ message: "Load state changed to 'En route to Delivery'" });
    } 
    if (load.state === 'En route to delivery') {
      await Load.findOneAndUpdate({ _id: load.id }, { state: 'Arrived to delivery', status: 'SHIPPED' });
      await Truck.findOneAndUpdate({ loadId: load.id, status: 'OL' }, { status: 'IS', loadId: null });

      return res.status(200).json({ message: "Load state changed to 'Arrived to delivery'" });
    }
    if (!load) {
      return res.status(400).json({ message: 'Bad request' });
    }
  } catch (err) {
    return res.status(500).send({ message: 'Server error' });
  }
};

const getLoad = async (req, res) => {
  try {
    const { userId } = req.user;
    const person = await User.findOne({ _id: userId });
    const load = await Load.findById(req.params.id);

    if (!userId || !person || !req.params.id) {
      return res.status(400).json({ message: 'Bad request' });
    }
    if ((load.status === 'ASSIGNED' && load.driverId === userId) || person.role === 'SHIPPER') {
      return res.status(200).json({ load });
    } else {
      return res.status(400).send({ message: 'You don`t have access to loads' });
    }
  } catch (err) {
    return res.status(500).send({ message: 'Server error' });
  }
};

const updateLoad = async (req, res) => {
  try {
    const { userId } = req.user;
    const shipper = await User.findOne({ _id: userId });
    if (shipper.role !== 'SHIPPER') {
      return res.status(400).json({ message: 'User is not a shipper' });
    }

    const load = await Load.findById(req.params.id);

    if (load.status === 'NEW') {
      const {
        name, payload, pickup_address, delivery_address, dimensions,
      } = req.body;
      const { width, length, height } = dimensions;

      if (!name || !payload || !pickup_address || !delivery_address || !dimensions
        || !width || !length || !height || !userId || typeof name !== 'string'
        || typeof payload !== 'number' || typeof pickup_address !== 'string' || typeof delivery_address !== 'string'
        || typeof dimensions !== 'object' || typeof width !== 'number' || typeof length !== 'number'
        || typeof height !== 'number') {
        return res.status(400).json({ message: 'Bad request' });
      }

      return await Load.findOneAndUpdate({ _id: req.params.id }, {
        name, payload, pickup_address, delivery_address, 'dimensions.width': width, 'dimensions.length': length, 'dimensions.height': height,
      })
        .then(() => res.status(200).json({ message: 'Load details changed successfully' }));
    }
    return res.status(400).json({ message: 'This load has not status "NEW"' });
  } catch (err) {
    return res.status(500).send({ message: 'Server error' });
  }
};

const deleteLoad = async (req, res) => {
  try {
    const { userId } = req.user;
    const shipper = await User.findOne({ _id: userId });
    if (shipper.role !== 'SHIPPER') {
      return res.status(400).json({ message: 'User is not a shipper' });
    }

    const load = await Load.findById(req.params.id);
    if (load.status === 'NEW') {
      await Load.findByIdAndDelete(req.params.id)
        .then(() => res.status(200).json({ message: 'Load deleted successfully' }));
    } else {
      return res.status(400).json({ message: 'This load has not status "NEW"' });
    }
  } catch (err) {
    return res.status(500).send({ message: 'Server error' });
  }
};

const postLoad = async (req, res) => {
  try {
    const { userId } = req.user;
    const shipper = await User.findOne({ _id: userId });
    if (shipper.role !== 'SHIPPER') {
      return res.status(400).json({ message: 'User is not a shipper' });
    }

    if (!userId || !req.params.id) {
      return res.status(400).json({ message: 'Bad request' });
    }

    const load = await Load.findById(req.params.id);
    if (load.status === 'ASSIGNED') {
      return res.status(400).json({ message: 'This load is already assigned' });
    }

    await Load.findOneAndUpdate({ _id: req.params.id }, { status: 'POSTED' });

    const truck = await Truck.findOne({
      payload: { $gt: load.payload },
      assigned_to: { $exists: true },
      'dimensions.width': { $gt: load.dimensions.width },
      'dimensions.length': { $gt: load.dimensions.length },
      'dimensions.height': { $gt: load.dimensions.height },
      status: 'IS',
    });

    if (truck) {
      await Truck.findOneAndUpdate({ _id: truck.id }, { status: 'OL', loadId: load.id });
      await Load.findOneAndUpdate({ _id: load.id }, {
        status: 'ASSIGNED', state: 'En route to Pick Up', assigned_to: truck.id, driverId: truck.assigned_to, logs: { message: `Load assigned to driver with id ${truck.assigned_to}`, time: new Date().toISOString() },
      });
      return res.status(200).send({
        message: 'Load posted successfully',
        driver_found: true,
      });
    }
    await Load.findOneAndUpdate({ _id: load.id }, { status: 'NEW' });
    return res.status(400).send({
      message: 'No drivers available',
    });
  } catch (err) {
    return res.status(500).send({ message: 'Server error' });
  }
};

const getShipInfo = async (req, res) => {
  try {
    const { userId } = req.user;
    const shipper = await User.findOne({ _id: userId });
    if (shipper.role !== 'SHIPPER') {
      return res.status(400).json({ message: 'User is not a shipper' });
    }

    const load = await Load.findOne({ _id: req.params.id, status: 'ASSIGNED' });
    const truck = await Truck.findById(load.assigned_to);

    if (!req.params.id || !load || !truck) {
      return res.status(400).json({ message: 'Bad request' });
    }
    return res.status(200).json({ load, truck });
  } catch (err) {
    return res.status(500).send({ message: 'Server error' });
  }
};

module.exports = {
  getAllLoads,
  addLoad,
  getActiveLoad,
  iterateToLoadState,
  getLoad,
  updateLoad,
  deleteLoad,
  postLoad,
  getShipInfo,
};
