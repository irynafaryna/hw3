const express = require('express');
const morgan = require('morgan');

const app = express();
const mongoose = require('mongoose');

mongoose.connect('mongodb+srv://Iryna:newpassword@cluster0.c1cziwy.mongodb.net/uber?retryWrites=true&w=majority');

const { authRouter } = require('./router/auth_router');
const { trucksRouter } = require('./router/trucks_router');
const { usersRouter } = require('./router/users_router');
const { loadsRouter } = require('./router/loads_router');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/trucks', trucksRouter);
app.use('/api/users/me', usersRouter);
app.use('/api/loads', loadsRouter);

const start = async () => {
  try {
    app.listen(8080);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

// ERROR HANDLER
// function errorHandler(err, req, res) {
//   console.error(err);
//   res.status(500).send({ message: 'Server error' });
// }

// app.use(errorHandler);
