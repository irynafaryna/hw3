const { User } = require('../models/Users');

const driverMiddleware = async (req, res) => {
  try {
    const { userId } = req.user;
    const driver = await User.findOne({ _id: userId });
    if (driver.role !== 'DRIVER') {
      res.status(400).json({ message: 'User is not a driver' });
    }
  } catch (err) {
    return res.status(500).json({ message: 'Server error' });
  }
  return 1;
};

module.exports = {
  driverMiddleware,
};
