const nodemailer = require('nodemailer');

const sendEmail = async (email, subject, password) => {
  const testAccount = await nodemailer.createTestAccount();

  try {
    const transporter = nodemailer.createTransport({
      host: 'smtp.ethereal.email',
      port: 587,
      secure: false, 
      auth: {
        user: testAccount.user, 
        pass: testAccount.pass, 
      },
    });

    const res = await transporter.sendMail({
      from: 'uber@gmail.com',
      to: email,
      subject,
      html: `<p>Password: ${password}</p>`,
    });

    console.log('Email link', nodemailer.getTestMessageUrl(res));

    console.log('email sent sucessfully');
  } catch (error) {
    console.log(error, 'email not sent');
  }
};

module.exports = sendEmail;